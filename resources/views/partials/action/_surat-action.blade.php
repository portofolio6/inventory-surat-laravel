<a href="javascript:void(0)" data-toggle="tooltip" onclick="editFuncSurat('{{ $id }}')" data-original-title="Edit" class="edit btn btn-success edit">
<i class="bx bx-edit-alt"></i>
</a>
<a href="javascript:void(0);" id="delete-surat" onclick="delFuncSurat('{{ $id }}')" data-toggle="tooltip" data-original-title="Delete" class="delete btn btn-danger">
<i class="bx bx-trash-alt"></i>
</a>