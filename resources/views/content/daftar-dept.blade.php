@extends('layouts.app')

@section('content')

    @include('partials._sidebar')

    <div class="home-section">
        @include('partials._nav')
        <div class="home-content">
            <div class="sales-boxes">
                <div class="card-content_full box">
                    <div class="position">
                        <h3>Daftar Surat</h3>
                        <a class="btn btn-primary" onClick="addDept()" href="javascript:void(0)"> <i class="bx bx-plus"></i> Department</a>
                    </div>
                    <br>
                    <table class="table table-hover" id="table-dept"></table>

                    <div class="modal fade" id="formModal">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="title-modal"></h5>
                                </div>
                                <div class="modal-body">
                                    <form action="javascript:void(0)" id="deptForm" name="deptForm" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="id" id="id">
                                        <div class="mb-4">
                                            <label for="nama_dept" class="form-label">Nama Dept</label>
                                            <input type="text" class="form-control" id="nama_dept" name="nama_dept" placeholder="Inputkan nama department">
                                            <div class="text-danger error-text nama_dept_err"></div>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" id="btn-save">Save</button>
                                    <a class="btn btn-danger" href="javascript:void(0)" id="btn-close"> Cancel</a>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
