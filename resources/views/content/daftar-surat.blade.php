@extends('layouts.app')

@section('content')

    @include('partials._sidebar')

    <div class="home-section">
        @include('partials._nav')
        <div class="home-content">
            <div class="sales-boxes">
                <div class="card-content_full box">
                    <div class="position">
                        <h3>Daftar Surat</h3>
                        <a class="btn btn-primary" onClick="addSurat()" href="javascript:void(0)"> <i class="bx bx-plus"></i> Surat</a>
                    </div>
                    <br>
                    <table class="table table-hover" id="table-surat"></table>

                    <div class="modal fade" id="formModal">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="title-modal"></h5>
                                </div>
                                <div class="modal-body">
                                    <form action="javascript:void(0)" id="suratForm" name="suratForm" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="id" id="id">
                                        <div class="mb-4">
                                            <label for="nama_surat" class="form-label">Judul Surat</label>
                                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Inputkan judul surat">
                                            <div class="text-danger error-text nama_err"></div>
                                        </div>
                                        <div class="mb-4">
                                            <label for="no_surat" class="form-label">No Surat</label>
                                            <input type="text" class="form-control" id="no_surat" name="no_surat" placeholder="Inputkan No Surat">
                                            <div class="text-danger error-text no_surat_err"></div>
                                        </div>
                                        <div class="row">
                                            <div class="mb-4 col-6">
                                                    <label for="user_id" class="form-label">Tujuan Surat</label>
                                                    <select class="form-control" id="user_id" name="user_id">
                                                        <option disabled selected value> -- Pilih Tujuan -- </option>
                                                        @foreach ($users as $user)
                                                            <option value="{{ $user->id }}">{{ $user->dept->nama_dept }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger error-text user_id_err"></div>
                                            </div>    
                                            <div class="mb-4 col-6">
                                                    <label for="kategori_id" class="form-label">Kategori Surat</label>
                                                    <select class="form-control" id="kategori_id" name="kategori_id">
                                                        <option disabled selected value> -- Pilih Kategori -- </option>
                                                        @foreach ($kategoris as $kategori)
                                                            <option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger error-text kategori_id_err"></div>
                                            </div>
                                        </div>    
                                        <div class="row">
                                            <div class="mb-4 col-6">
                                                <label for="jenis_surat" class="form-label">Jenis Surat</label>
                                                <select class="form-control" id="jenis_surat" name="jenis_surat" >
                                                    <option disabled selected value> -- Pilih Jenis -- </option>
                                                    <option value="masuk">Masuk</option>
                                                    <option value="keluar">Keluar</option>
                                                </select>
                                                <div class="text-danger error-text jenis_surat_err"></div>
                                            </div>
                                            <div class="mb-4 col-6">
                                                <label for="tanggal_terima" class="form-label">Tanggal</label>
                                                <input type="date" class="form-control" id="tanggal_terima" name="tanggal_terima">
                                                <div class="text-danger error-text tanggal_terima_err"></div>
                                            </div>
                                        </div>
                                        <div class="mb-4">
                                            <label for="perihal" class="form-label">Perihal</label>
                                            <input type="text" class="form-control" id="perihal" name="perihal" placeholder="Inputkan Perihal">
                                            <div class="text-danger error-text perihal_err"></div>
                                        </div>
                                        <div class="mb-4">
                                            <label for="file_surat" class="form-label">Upload File</label>
                                            <input type="file" class="form-control" id="file_surat" name="file_surat" placeholder="Upload File">
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" id="btn-save">Save</button>
                                    <a class="btn btn-danger" href="javascript:void(0)" id="btn-close"> Cancel</a>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
