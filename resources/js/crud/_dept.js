const { default: Swal } = require("sweetalert2");
const { result } = require("lodash");

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
    });
    $('#table-dept').DataTable({
        serverSide: true,
        processing: true,
        ajax: '/department',
        columns: [{
                data: 'id',
                className: 'text-center fit',
                title: 'No.',
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'nama_dept',
                className: 'text-left fit',
                title: 'Nama Department'
            },
            {
                data: 'action',
                className: 'text-center fit',
                title: 'Action',
            },
        ]
    });
});

addDept = function() {
    $('#deptForm').trigger('reset');
    $('#title-modal').html('Tambah Department');
    $('#formModal').modal('show');
    $('#id').val('');
    clearErrors();
}

$('#btn-close').click(function() {
    $('#formModal').hide();
})

editFuncDept = function(id) {
    clearErrors()
    $.ajax({
        type: 'POST',
        url: '/department/edit-dept',
        data: {
            id: id,
        },
        dataType: 'json',
        success: function(res) {
            $('#title-modal').html('Edit Department')
            $('#formModal').modal('show')
            $('#id').val(res.id)
            $('#nama_dept').val(res.nama_dept)
        },
    })
}

delFuncDept = function(id) {
    var id = id
    Swal.fire({
        title: 'Apakah Anda Yakin?',
        text: 'Data Department akan di hapus!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ya, hapus!'
    }).then((result) => {
        if (result.isConfirm) {
            $.ajax({
                type: 'POST',
                url: '/deparment/delete-dept',
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function(res) {
                    var oTable = $('#table-dept').DataTable();
                    oTable.fnDraw(false);
                }
            });
            Swal.fire(
                'Deleted!',
                'Data department berhasil di hapus.',
                'success'
            )
        }
    })
}

$('#deptForm').submit(function(e) {
    e.preventDefault();
    var formData = new formData(this);
    $.ajax({
        type: 'POST',
        url: '/department/store-dept',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data) => {
            $('#formModal').modal('hide');
            Swal.fire({
                icon: 'success',
                title: 'Data berhasil di simpan.',
                showConfirmButton: false,
                timer: 1500
            })
            var oTable = $('#table-dept').DataTable();
            oTable.fnDraw(false);
            $('#btn-save').html('Submit');
            $('#btn-save').attr('disabled', false);
        },
        error: function(res) {
            if (res.responseJSON.errors) {
                printErrorMsg(res.responseJSON.errors);
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Error',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        }
    });
});

function printErrorMsg(msg) {
    $.each(msg, function(key, value) {
        $('.' + key + '_err').text(value[0]);
    });
}

function clearErrors() {
    const errorMessages = document.querySelectorAll('.text-danger')
    errorMessages.forEach((element) => element.textContent = '')
}