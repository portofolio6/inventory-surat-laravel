const { result } = require("lodash");
const { default: Swal } = require("sweetalert2");

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
    });
    $('#table-kategori').DataTable({
        serverSide: true,
        processing: true,
        ajax: '/kategori',
        columns: [{
                data: 'id',
                className: 'text-center fit',
                title: 'No.',
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'nama',
                className: 'text-left fit',
                title: 'Nama Kategori'
            },
            {
                data: 'action',
                className: 'text-center fit',
                title: 'Action',
            },
        ]
    });
});

addKategori = function() {
    $('#kategoriForm').trigger('reset');
    $('#title-modal').html('Tambah Kategori');
    $('#formModal').modal('show');
    $('#id').val('');
    clearErrors();
}

$('#btn-close').click(function() {
    $('#formModalKategori').hide();
})

editFuncKategori = function(id) {
    clearErrors()
    $.ajax({
        type: 'POST',
        url: '/kategori/edit-kategori',
        data: {
            id: id,
        },
        dataType: 'json',
        success: function(res) {
            $('#title-modal').html('Edit Kategori')
            $('#formModal').modal('show')
            $('#id').val(res.id)
            $('#nama').val(res.nama)
        },
    })
}

delFuncKategori = function(id) {
    var id = id
    Swal.fire({
        title: 'Apakah Anda Yakin?',
        text: 'Data kategori akan di hapus!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ya, hapus!'
    }).then((result) => {
        if (result.isConfirm) {
            $.ajax({
                type: 'POST',
                url: '/kategori/delete-kategori',
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function(res) {
                    var oTable = $('#table-kategori').DataTable();
                    oTable.fnDraw(false);
                }
            });
            Swal.fire(
                'Deleted!',
                'Data kategori berhasil di hapus.',
                'success'
            )
        }
    });
}

$('#kategoriForm').submit(function(e) {
    e.preventDefault();
    var formData = new formData(this);
    $.ajax({
        type: 'POST',
        url: '/kategori/store-kategori',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data) => {
            $('#formModal').modal('hide');
            Swal.fire({
                icon: 'success',
                title: 'Data berhasil di simpan.',
                showConfirmButton: false,
                timer: 1500
            })
            var oTable = $('#table-kategori').DataTable();
            oTable.fnDraw(false);
            $('#btn-save').html('Submit');
            $('#btn-save').attr('disabled', false);
        },
        error: function(res) {
            if (res.responseJSON.errors) {
                printErrorMsg(res.responseJSON.errors);
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Error',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        }
    });
});

function printErrorMsg(msg) {
    $.each(msg, function(key, value) {
        $('.' + key + '_err').text(value[0]);
    });
}

function clearErrors() {
    const errorMessages = document.querySelectorAll('.text-danger')
    errorMessages.forEach((element) => element.textContent = '')
}