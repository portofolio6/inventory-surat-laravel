const { type } = require('jquery');

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
    })

    $('#table-surat').DataTable({
        serverSide: true,
        processing: true,
        ajax: '/surat',
        columns: [{
                data: 'id',
                className: 'text-center fit',
                title: 'No',
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'nama',
                className: 'text-left fit',
                title: 'Nama Surat',
            },
            {
                data: 'no_surat',
                className: 'text-left fit',
                name: 'no_surat',
                title: 'Nomer Surat',
            },
            {
                data: 'user.dept.nama_dept',
                className: 'text-left fit',
                title: 'Tujuan',
            },
            {
                data: 'perihal',
                className: 'text-left fit',
                title: 'Perihal',
            },
            {
                data: null,
                className: 'text-left fit ',
                title: 'Jenis Surat',
                render: function(data) {
                    if (data.jenis_surat === "masuk") {
                        jenis_surat = '<td><span class="masuk jenis_surat">' + data.jenis_surat + '</span></td>';
                    } else {
                        jenis_surat = '<td><span class="keluar jenis_surat">' + data.jenis_surat + '</span></td>';
                    }
                    return jenis_surat
                }
            },

            {
                data: 'action',
                className: 'text-left fit',
                title: 'Action',
            },
        ],
    });
});

addSurat = function() {
    $('#suratForm').trigger('reset')
    $('#title-modal').html('Tambah Surat')
    $('#formModal').modal('show')
    $('#id').val('')
    clearErrors()
}

jQuery('#btn-close').click(function($) {
    jQuery('#formModal').modal('hide')
})

editFuncSurat = function(id) {
    clearErrors()
    $.ajax({
        type: 'POST',
        url: '/surat/edit-surat',
        data: {
            id: id,
        },
        dataType: 'json',
        success: function(res) {
            $('#title-modal').html('Edit Surat')
            $('#formModal').modal('show')
            $('#id').val(res.id)
            $('#nama').val(res.nama)
            $('#no_surat').val(res.no_surat)
            $('#user_id').val(res.user_id)
            $('#kategori_id').val(res.kategori_id)
            $('#tanggal_terima').val(res.tanggal_terima)
            $('#perihal').val(res.perihal)
            $('#jenis_surat').val(res.jenis_surat)
            $('#file_surat').val(res.file_surat)
        },
    })
}

delFuncSurat = function(id) {
    var id = id
    swal.fire({
        title: 'Apakah Anda Yakin?',
        text: "Data surat akan di hapus!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: '/surat/delete-surat',
                data: {
                    id: id,
                },
                dataType: 'json',
                success: function(res) {
                    var oTable = $('#table-surat').dataTable();
                    oTable.fnDraw(false);
                }
            });
            swal.fire(
                'Deleted!',
                'Data surat berhasil di hapus.',
                'success'
            )
        }
    })
}

$('#suratForm').submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: 'POST',
        url: "/surat/store-surat",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data) => {
            $("#formModal").modal('hide');
            swal.fire({
                icon: 'success',
                title: 'Data berhasil di simpan',
                showConfirmButton: false,
                timer: 1500
            })
            var oTable = $('#table-surat').dataTable();
            oTable.fnDraw(false);
            $("#btn-save").html('Submit');
            $("#btn-save").attr("disabled", false);
        },
        error: function(res) {
            if (res.responseJSON.errors) {
                printErrorMsg(res.responseJSON.errors);
            } else {
                swal.fire({
                    icon: 'error',
                    title: 'Terjadi Erorr',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        }
    });
});

function printErrorMsg(msg) {
    $.each(msg, function(key, value) {
        $('.' + key + '_err').text(value[0]);
    });
}

function clearErrors() {
    // remove all error messages
    const errorMessages = document.querySelectorAll('.text-danger')
    errorMessages.forEach((element) => element.textContent = '')
}