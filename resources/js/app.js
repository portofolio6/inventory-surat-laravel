import 'datatables/media/css/jquery.dataTables.css';
import 'datatables';
const swal = window.swal = require('sweetalert2');

require('./bootstrap');
require('./partials/_button');
require('./crud/_surat');
require('./crud/_dept');
require('./crud/_kategori');