<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|regex:/^[a-zA-Z]+$/u|string|max:255',
            'no_surat' => 'required|string',
            'perihal' => 'required|string',
            'user_id' => 'required',
            'kategori_id' => 'required',
            'jenis_surat' => 'required',
            'tanggal_terima' => 'required',
            'file_surat' => 'required|mimes:pdf,docx|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Nama surat harus di Isi',
            'no_surat.required' => 'No. surat harus di Isi',
            'jenis_surat.required' => 'Jenis surat harus di Isi',
            'tanggal_terima.required' => 'Tanggal harus di Isi',
            'user_id.required' => 'Tujuan harus di Isi',
            'kategori_id.required' => 'Kategori harus di Isi',
            'file_surat.required' => 'File surat harus di isi'
        ];
    }
}
