<?php

namespace App\Http\Controllers;

use App\Http\Requests\KategoriRequest;
use App\Repository\Kategori\QueryKategoriRepository;
use Illuminate\Http\Request;
use Datatables;

class KategorisController extends Controller
{
    protected $queryKategoriRepository;

    public function __construct(QueryKategoriRepository $queryKategoriRepository)
    {
        $this->queryKategoriRepository = $queryKategoriRepository;
    }

    public function index()
    {
        $kategori = $this->queryKategoriRepository->getKategori();
        return $kategori;
    }

    public function listKategori()
    {
        $kategori = $this->queryKategoriRepository->getKategori();
        if (request()->ajax()) {
            return DataTables()->of($kategori)
                ->addColumn('action', 'partials.action._kategori-action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('content.daftar-kategori');
    }

    public function edit(Request $request)
    {
        try {
            return $this->queryKategoriRepository->getKategoriById($request);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function store(KategoriRequest $request)
    {
        try {
            return $this->queryKategoriRepository->storeKategori($request);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function destroy(Request $request)
    {
        return $this->queryKategoriRepository->deleteKategori($request);
    }
}
