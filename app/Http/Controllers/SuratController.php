<?php

namespace App\Http\Controllers;

use App\Http\Requests\SuratRequest;
use App\Repository\Surat\QuerySuratRepository;
use App\Repository\Kategori\QueryKategoriRepository;
use App\Repository\User\QueryUserRepository;
use Illuminate\Http\Request;
use Datatables;

class SuratController extends Controller
{
    protected $queryRepository;
    protected $queryKategoriRepository;
    protected $queryUserRepository;

    public function __construct(QuerySuratRepository $queryRepository, QueryKategoriRepository $queryKategoriRepository, QueryUserRepository $queryUserRepository)
    {
        $this->queryRepository = $queryRepository;
        $this->queryKategoriRepository = $queryKategoriRepository;
        $this->queryUserRepository = $queryUserRepository;
    }

    public function index()
    {
        $surat = $this->queryRepository->getSurat();
        $kategoris = $this->queryKategoriRepository->getKategori();
        $users = $this->queryUserRepository->getUserNotAdmin();
        if (request()->ajax()) {
            return DataTables()->of($surat)
                ->addColumn('action', 'partials.action._surat-action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('content.daftar-surat', compact(['kategoris', 'users']));
    }

    public function edit(Request $request)
    {
        try {
            return $this->queryRepository->getsuratById($request);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function store(SuratRequest $request)
    {
        try {
            return $this->queryRepository->storeSurat($request);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function destroy(Request $request)
    {
        return $this->queryRepository->deleteSurat($request);
    }
}
