<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeptRequest;
use Illuminate\Http\Request;
use App\Repository\Department\QueryDeptRepository;
use Datatables;

class DeptController extends Controller
{
    protected $queryDeptRepository;

    public function __construct(QueryDeptRepository $queryDeptRepository)
    {
        $this->queryDeptRepository = $queryDeptRepository;
    }

    public function index()
    {
        $dept = $this->queryDeptRepository->getDept();
        if (request()->ajax()) {
            return DataTables()->of($dept)
                ->addColumn('action', 'partials.action._dept-action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('content.daftar-dept');
    }

    public function edit(Request $request)
    {
        try {
            return $this->queryDeptRepository->getDeptById($request);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function store(DeptRequest $request)
    {
        try {
            return $this->queryDeptRepository->storeDept($request);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function destroy(Request $request)
    {
        return $this->queryDeptRepository->deleteDept($request);
    }
}
