<?php

namespace App\Repository\Department;

use Illuminate\Http\Request;

interface DeptRepository
{
    public function getDept();
    public function getDeptById($request);
    public function storeDept($request);
    public function deleteDept($request);
}
