<?php

namespace App\Repository\Department;

use App\Models\Department;

class QueryDeptRepository implements DeptRepository
{
    public function getDept()
    {
        $dept = Department::get();
        if (!$dept) {
            return null;
        }
        return $dept;
    }

    public function getDeptById($request)
    {
        $id = array('id' => $request->id);
        $dept = Department::where($id)->first();

        return response()->json($dept);
    }

    public function storeDept($request)
    {
        $id = $request->id;
        $dept = Department::updateOrCreate(
            ['id' => $id],
            [
                'nama_dept' => $request->nama_dept
            ]
        );

        return response()->json($dept);
    }

    public function deleteDept($request)
    {
        $dept = Department::find($request->id);
        $dept->delete();
        
        return response()->json($dept);
    }
}
