<?php

namespace App\Repository\Surat;

use Illuminate\Http\Request;

interface SuratRepository
{
    public function getSurat();
    public function getsuratById($request);
    public function storeSurat($request);
    public function deleteSurat($request);
}
