<?php

namespace App\Repository\Surat;

use App\Models\Surat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuerySuratRepository implements SuratRepository
{
    public function getSurat()
    {
        $surat = Surat::with('user.dept')->get();
        if (!$surat) {
            return null;
        }
        return $surat;
    }

    public function getsuratById($request)
    {
        $id = array('id' => $request->id);
        $surat = Surat::where($id)->first();
      
        return response()->json($surat);
    }

    public function storeSurat($request)
    {
        $id = array('id' => $request->id);
        $file_surat = $request->file('file_surat');
        $save = Storage::disk('public')->put('files', $file_surat);
        
        $surat = Surat::updateOrCreate(
            ['id' => $id],
            [
                'nama' => $request->nama,
                'user_id' => $request->user_id,
                'kategori_id' => $request->kategori_id,
                'no_surat' => $request->no_surat,
                'jenis_surat' => $request->jenis_surat,
                'tanggal_terima' => $request->tanggal_terima,
                'perihal' => $request->perihal,
                'file_surat' => $save,
                'status' => 'T'
            ]
        );

        return response()->json($surat);
    }

    public function deleteSurat($request)
    {
        $surat = Surat::find($request->id);
        $surat->delete();

        return response()->json($surat);
    }
}
