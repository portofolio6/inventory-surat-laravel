<?php

namespace App\Repository\User;

use App\Models\User;
use Illuminate\Http\Request;

class QueryUserRepository implements UserRepository
{
    public function getUserNotAdmin()
    {
        $user = User::with('dept')->where('role', '0')->get();
        if (!$user) {
            return null;
        }
        return $user;
    }
}
