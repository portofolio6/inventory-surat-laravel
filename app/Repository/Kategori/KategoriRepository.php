<?php

namespace App\Repository\Kategori;

use Illuminate\Http\Request;

interface KategoriRepository
{
    public function getKategori();
    public function getKategoriById($request);
    public function storeKategori($request);
    public function deleteKategori($request);
}
