<?php

namespace App\Repository\Kategori;

use App\Models\Kategoris;
use Illuminate\Http\Request;

class QueryKategoriRepository implements KategoriRepository
{
    public function getKategori()
    {
        $kategori = Kategoris::get();
        if (!$kategori) {
            return null;
        }
        return $kategori;
    }

    public function getKategoriById($request)
    {
        $id = array('id' => $request->id);
        $kategori = Kategoris::where('id', $id)->first();

        return response()->json($kategori);
    }

    public function storeKategori($request)
    {
        $id = $request->id;
        $kategori = Kategoris::updateOrCreate(
            ['id' => $id],
            [
                'nama' => $request->nama
            ]
        );

        return response()->json($kategori);
    }

    public function deleteKategori($request)
    {
        $kategori = Kategoris::find($request->id);
        $kategori->delete();

        return response()->json($kategori);
    }
}
