<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SuratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 10 ; $i++) {
            DB::table('surats')->insert([
                'nama_surat' => 'surat-'.$i,
                'no_surat' => Str::random(3).'-'.Str::random(3).'-'.Str::random(3),
                'jenis_surat' => 'masuk',
                'id_detail_surat' => 1
            ]);
        }

    }
}
