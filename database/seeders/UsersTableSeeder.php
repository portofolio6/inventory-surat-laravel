<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Tata Usaha',
            'nip' => '1234567890',
            'password' => '12345',
            'role' => '0',
            'dept_id' => 2
        ]);
    }
}
