<?php

use App\Http\Controllers\DeptController;
use App\Http\Controllers\KategorisController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SuratController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('content.dashboard');
});
Route::get('/daftar-table', function () {
    return view('content.daftar-surat');
});

Route::prefix('surat')->group(function () {
    Route::get('', [SuratController::class, 'index']);
    Route::post('/edit-surat', [SuratController::class, 'edit']);
    Route::post('/store-surat', [SuratController::class, 'store']);
    Route::post('/delete-surat', [SuratController::class, 'destroy']);
});

Route::prefix('department')->group(function () {
    Route::get('', [DeptController::class, 'index']);
    Route::post('/edit-dept', [DeptController::class, 'edit']);
    Route::post('/store-dept', [DeptController::class, 'store']);
    Route::post('/delete-dept', [DeptController::class, 'destroy']);
});

Route::prefix('kategori')->group(function () {
    Route::get('', [KategorisController::class, 'listKategori']);
    Route::post('/edit-kategori', [KategorisController::class, 'edit']);
    Route::post('/store-kategori', [KategorisController::class, 'store']);
    Route::post('/delete-kategori', [KategorisController::class, 'destroy']);
});
